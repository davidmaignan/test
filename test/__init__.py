# import qgis libs so that ve set the correct sip api version
import qgis   # pylint: disable=W0611  # NOQA
import unittest

from test.test_init import TestInit
from test.test_resources import TestResourceTest


def suite_configuration():
    suite = unittest.TestSuite()
    suite.addTest(TestInit('test_read_init'))
    suite.addTest(TestResourceTest('test_icon_png'))
    return suite


if __name__ == '__main__':
    unittest.TextTestRunner(verbosity=1).run(suite_configuration())